// checking
console.log('Hello from JS');

// error
console.error('Error something');

// warning
console.warn('Ooops Warning');

// syntax:
	// function nameOfFunction() {
	// 	// instruction/procedures
	// }

function errorMessage() {
	console.error('Error oh No!!!');
}

errorMessage();


function greetings() {
	// procedures
	console.log('Salutations from JS');
}

// invocation -start the function by calling it out
greetings();

// diff methods in declaring a func in JS

// 1. [SECTION] variables declared outside a func can be used inside a func

let givenName = 'John'
let familyName = 'Smith'

// function for the above

function fullName() {
	console.log(givenName + ' ' + familyName);
}

fullName();


// importat note: Declaration before Invocation

// [SECTION]  "blocked sccope" var
// 

function  computeTotal () {
	let numA = 20;
	let numB = 5;

	console.log(numA + numB);
}

computeTotal ();

// [SECTION] Functions with Parameters
// "parameter"- var or container/catchers tat exist inside func. store info provided to a func when called or invoked

function pokemon(pangalan) {
	// use paramter declared to display inside
	console.log('I Choose you: ' + pangalan);
}

pokemon('Pikachu');

// parameters VS arguments?

// Argument - actual value provided inside a func  to work properly. the TERM argument is used when func are called/invoked as compared to parameters when a func is  simply declared.


// [SECTION] Functions with Multiple parameters

function addNumbers (numA, numB, numC, numD, numE) {
	console.log(numA + numB + numC + numD + numE);
}

addNumbers(1,2,3,4,5);

//function to create full name
function createFullName(fName, mName, lName) {
	console.log(lName+', '+fName+' '+mName);

}

createFullName('Juan','Dela','Cruz');


// [SECTION] Variable as Arguments

let attackA  = 'Tackle';
let attackB  = 'Thunderbolt';
let	attackC  = 'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack){
	console.log(name+' use '+attack);
}

pokemonStats(selectedPokemon, attackC);
// Ratata use Quick attack

// using var as arguments will allow us to utilize code reusablitity, to help reduce amount of code we have written down.


// the use of the "return" expression/statement
// the return statement is used to display the output of function


// [SECTION]  Return Statement

// return string message
	// body...
function returnString() {
	'Hello World'
	return 2 + 1;
}

// repackage resukt of this func inside a new variable
let functionOutput = returnString();
console.log(functionOutput);

function dialog() {
	console.log('Oops! I did it Again');
	console.log("Don't you know that youre toxic");
	'Im a Slave for you';
	return console.log('Sometime I run!');
}

// any block/line of code that will come after our return will be ignored. becuase it came after  the functin


// invocation
console.log(dialog());


//[SECTION] func as arguments
// func panrameters can aso accept other func as their arguments
// some complex func use oother functions as their argunments to perform more complicated/complex results

function argumentSaFunction()  {
	console.log('This func passed as argument');
}

function invokeFunction(argumentNaFunction, pangalawangFUnction) {
	argumentNaFunction();
	pangalawangFUnction(selectedPokemon, attackC);
	pangalawangFUnction(selectedPokemon, attackB);
}

invokeFunction(argumentSaFunction, pokemonStats);

console.log(invokeFunction); 
// find info about funct inside console